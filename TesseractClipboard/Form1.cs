﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using IronOcr;

namespace TesseractClipboard
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        public static string imagePath = null;
        public static Timer timer1 = null;
        public static int TIMER_INTERVAL = 2000; //ms
        IronTesseract IronOcr = new IronTesseract();

        private void updateRuntimeLabel()
        {
            var currentTime = label3.Text.Replace("Runtime: ", "");
            var newTimeMS = Int32.Parse(currentTime)*1000 + TIMER_INTERVAL;
            var newTimeSec = newTimeMS / 1000;
            label3.Text = "Runtime: " + newTimeSec.ToString();
        }

        private void analyzeClipboard()
        {

            {
                Image returnImage = null;
                if (Clipboard.ContainsImage())
                {
                    returnImage = Clipboard.GetImage();
                    var ocrText = IronOcr.Read(returnImage);
                    if (ocrText != null)
                    {
                        Clipboard.SetText(ocrText.Text);
                        textBox1.Text = ocrText.Text;
                    }
                }
            }
        }


        private void timer1_Tick(object sender, EventArgs e)
        {
            //MessageBox.Show("test");
            updateRuntimeLabel();
            analyzeClipboard();
        }

        public void InitTimer() {


            Console.WriteLine("Called InitTimer");
            timer1 = new Timer();
            timer1.Tick += new EventHandler(timer1_Tick);
            timer1.Interval = TIMER_INTERVAL; // in miliseconds
            timer1.Start();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            OpenFileDialog open = new OpenFileDialog();
            // image filters  
            open.Filter = "Image Files(*.jpg; *.jpeg; *.gif; *.bmp, *.png)|*.jpg; *.jpeg; *.gif; *.bmp; *.png";
            if (open.ShowDialog() == DialogResult.OK)
            {
                // display image in picture box  
                pictureBox1.Image = new Bitmap(open.FileName);
                // image file path  
                
                 imagePath = open.FileName;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var Result = IronOcr.Read(imagePath);
            textBox1.Text = Result.Text;
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
        private void Form1_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e) {

            if (timer1 == null)
            {
                button3.Text = "Stop Clipboard Analyzer";
                label3.Text = "Runtime: 0";
                InitTimer();
                label2.Text = "Status: On";

            } else
            {
                button3.Text = "Start Clipboard Analyzer";
                timer1.Stop();
                timer1 = null;
                label2.Text = "Status: Off";
            }
            
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {
            // Runtime
        }

        private void label2_Click(object sender, EventArgs e)
        {
            // Status

        }
    }
}
