# Tesseract Clipboard

This .Net app lets you convert images to text and run a background process that will convert images in your clipboard into text.

IronOCR is not free. If you have a license issue, please note you must run this from a development environment. Otherwise, throw me $399 and I'll buy a license.
